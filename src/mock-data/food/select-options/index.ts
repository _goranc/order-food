export const MAIN_DISHES_OPTION = [
    { value: 'Первые блюда', label: 'Первые блюда' },
    { value: 'Салаты', label: 'Салаты' },
    { value: 'Гарниры', label: 'Гарниры' },
    { value: 'Мясные', label: 'Мясные' },
    { value: 'Рыбные', label: 'Рыбные' },
    { value: 'Закуски', label: 'Закуски' },
  ]