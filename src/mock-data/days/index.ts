export enum EDay {
  MONDAY = 'MONDAY',
  TUESDAY = 'TUESDAY',
  WEDNESDAY = 'WEDNESDAY',
  THURSDAY = 'THURSDAY',
  FRIDAY = 'FRIDAY',
}

export type TDayAbbrvtn = {
  [key in EDay]: 'Пн' | 'Вт' | 'Ср' | 'Чт' | 'Пт'
}

export const DAY_ABBREVIATION: TDayAbbrvtn = {
  MONDAY: 'Пн',
  TUESDAY: 'Вт',
  WEDNESDAY: 'Ср',
  THURSDAY: 'Чт',
  FRIDAY: 'Пт',
}
