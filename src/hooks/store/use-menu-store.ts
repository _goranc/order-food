import * as Redux from 'redux'
import { TMenuItem } from '../../types/menu-item'
import { actionsBasket } from '../../redux/actions/basketAction'
import { menuActions } from '../../redux/actions/menuAction'
import { useDispatch, useSelector } from 'react-redux'
import { getActiveDay } from '../../redux/selectors'
import { RootActionsType } from '../../redux/redux-store'
import { useCallback } from 'react'

export const useMenuStore = () => {
  const { addToBasket, removeFromBasket } = actionsBasket
  const { changeMenuCounter, toogleChecked } = menuActions
  const activeDay = useSelector(getActiveDay)

  const dispatch = useDispatch<Redux.Dispatch<RootActionsType>>()

  const removeMenuItem = useCallback(
    (menuItem: TMenuItem) => {
      dispatch(removeFromBasket(menuItem.name))
      dispatch(toogleChecked(menuItem))
      dispatch(changeMenuCounter(menuItem.name, 1))
    },
    [dispatch]
  )

  const addMenuItem = useCallback(
    (menuItem: TMenuItem) => {
      dispatch(addToBasket(menuItem, activeDay))
      dispatch(toogleChecked(menuItem))
    },
    [dispatch]
  )

  return {
    removeMenuItem,
    addMenuItem,
  }
}
