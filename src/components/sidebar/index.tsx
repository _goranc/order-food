import * as React from 'react'
import styled from 'styled-components'
import colors from '../../shared/styles/colors'
import MenuItem, { MenuItemWrapper } from '../../shared/ui-elements/buttons/sidebar'
import { MENU_ITEMS } from '../../constants'
import { ActiveDayContext } from '../../shared/context/active-day'

const SidebarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100vh;
  max-width: 300px;
  background-color: ${colors.pink500};
  box-shadow: 0px 4px 6px ${colors.gray500};
  overflow-y: auto;

  ${MenuItemWrapper} {
    margin: 24px 0;

    ::first-child {
      margin-top: 0;
    }

    :last-child {
      margin-bottom: 0;
    }
  }

  @media (max-width: 800px) {
    display: none;
  }
`

const LogoWrapper = styled.div`
  margin-top: 64px;

  img {
    object-fit: cover;
    width: 160px;
  }
`

const MenuItemsWrapper = styled.div`
  margin-top: calc(50% - 32px);
  margin-bottom: auto;
`

const Sidebar: React.FC = () => {
  const { activeDay, setActiveDay } = React.useContext(ActiveDayContext)

  return (
    <SidebarWrapper>
      <LogoWrapper>
        <img src='/logo.png' alt='logo' />
      </LogoWrapper>
      <MenuItemsWrapper>
        {MENU_ITEMS.map(({ id, label }) => (
          <MenuItem
            label={label}
            key={id}
            isActive={id === activeDay}
            onClick={() => setActiveDay(id)}
          />
        ))}
      </MenuItemsWrapper>
    </SidebarWrapper>
  )
}

export default Sidebar
