import styled from 'styled-components'

interface IPaginationWrapper {
  onClick: (event: React.MouseEvent<HTMLElement>) => void
}

export const MenuItems = styled.ul`
  height: 70vh;
  overflow: auto;
  padding: 0 1rem;
  margin: 0 auto;
  @media (max-width: 800px) {
    height: 65vh;
  }
`

export const PaginationWrapper = styled.div<IPaginationWrapper>`
  display: flex;
  justify-content: center;
  padding: 10px 0;
`
