import * as React from 'react'
import { Wrapper, FieldName, WrapperCheck, WrapperValue } from './views'
import CheckBox from '../../../shared/ui-elements/checkboxes/CheckBox'
import { TMenuItem } from '../../../types/menu-item'
import { Counter } from '../../Counter'
import { useMenuStore } from '../../../hooks/store/use-menu-store'

type TProps = {
  menuItem: TMenuItem
}

const MenuItem: React.FC<TProps> = ({ menuItem }) => {
  const { name, isChecked, menu_id, price } = menuItem

  const { addMenuItem, removeMenuItem } = useMenuStore()

  return (
    <Wrapper>
      <WrapperCheck>
        <CheckBox
          name={name}
          onChange={() => (isChecked ? removeMenuItem(menuItem) : addMenuItem(menuItem))}
          checked={isChecked}
        ></CheckBox>
      </WrapperCheck>
      <WrapperValue>
        <p>
          <FieldName>id: </FieldName>
          <strong>{menu_id}</strong>
        </p>
        <p>
          <FieldName>price: </FieldName>
          <strong>{price} uah</strong>
        </p>
      </WrapperValue>
      <Counter name={menuItem.name} counter={menuItem.counter} isBasket={false} />
    </Wrapper>
  )
}

export default MenuItem
