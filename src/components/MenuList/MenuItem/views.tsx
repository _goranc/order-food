import styled from 'styled-components'
import colors from '../../../shared/styles/colors'

export const Wrapper = styled.li`
  position: relative;
  min-width: 550px;
  width: 60vw;
  display: flex;
  align-items: center;
  padding: 1rem 1rem;
  margin-bottom: 1rem;
  &:last-child {
    margin-bottom: 0;
  }
  height: 4rem;
  background: rgba(229, 253, 253, 0.9);
  color: ${colors.dark500};
  border-radius: 6px;
  line-height: 1.2rem;
  @media (max-width: 800px) {
    width: 90vw;
    min-width: 400px;
  }
`

export const WrapperCheck = styled.label`
  width: 100%;
  padding-left: 0.8rem;
`
export const FieldName = styled.span`
  padding-left: 1rem;
  color: rgba(51, 51, 51, 0.5);
`

export const WrapperValue = styled.div`
  width: 260px;
  min-width: 160px;
  display: flex;

  @media (max-width: 800px) {
    flex-direction: column;
    width: 160px;
    min-width: 100px;
  }
`
