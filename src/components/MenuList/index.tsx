import * as React from 'react'
import * as Redux from 'redux'
import MenuItem from './MenuItem'
import { useSelector, useDispatch } from 'react-redux'
import { MenuItems, PaginationWrapper } from './views'
import { TMenuItem } from '../../types/menu-item'
import Pagination from '../Pagination'
import { menuActions } from '../../redux/actions/menuAction'
import { RootActionsType } from '../../redux/redux-store'
import { getMenuItemsFiltred, getPage } from '../../redux/selectors'

const itemPerPage: number = 12

const getCurrentPageItems = (menu: TMenuItem[], page: number) => {
  const indexOfLastItem = page * itemPerPage
  const indexOfFirstItem = indexOfLastItem - itemPerPage
  return menu.slice(indexOfFirstItem, indexOfLastItem)
}

const MenuList: React.FC = () => {
  const { changePage } = menuActions

  const dispatch = useDispatch<Redux.Dispatch<RootActionsType>>()
  const page = useSelector(getPage)
  const foodMenu = useSelector(getMenuItemsFiltred)

  const lastPage = Math.ceil(foodMenu.length / itemPerPage)

  const onClickHandler = (event: React.MouseEvent<HTMLElement>) => dispatch(changePage(event, page))

  return (
    <>
      <MenuItems>
        {getCurrentPageItems(foodMenu, page).map((item: TMenuItem, id: number) => (
          <MenuItem key={id} menuItem={item} />
        ))}
      </MenuItems>
      <PaginationWrapper onClick={onClickHandler}>
        <Pagination lastPage={lastPage} activePage={page}></Pagination>
      </PaginationWrapper>
    </>
  )
}

export default MenuList
