import * as React from 'react'
import styled from 'styled-components'
import colors from '../../shared/styles/colors'

const COLORS = {
  active: 'rgba(111, 111, 111, 0.19)',
  default: 'rgba(214, 214, 214, 0.1)',
}

interface IPagination {
  isActive?: boolean
}
interface IPaginationProps {
  lastPage: number
  activePage: number
}

const PaginationLink = styled.a<IPagination>`
  color: ${colors.dark500};
  border-radius: 6px;
  width: 2.4rem;
  height: 2.4rem;
  text-align: center;
  box-shadow: 0px 1px 4px ${colors.gray500};
  line-height: 2.5rem;
  margin: 0 0.2rem;
  text-decoration: none;
  background-color: ${({ isActive }) => (isActive ? COLORS.active : COLORS.default)};
`
const PaginationButton = styled.i`
  color: #757575;
  width: 2rem;
  height: 2rem;
  text-align: center;
  line-height: 2.5rem;
`

const renderPagination = (lastPage: number, activePage: number) => {
  let pagination: Array<React.ReactNode> = []

  const displayedPageNumbers = 2

  let startPagination: number = 1
  let finishPagination: number = lastPage

  if (activePage <= displayedPageNumbers + 1) {
    finishPagination =
      lastPage > displayedPageNumbers * 2 ? startPagination + displayedPageNumbers * 2 : lastPage
  } else if (
    activePage > displayedPageNumbers + 1 &&
    lastPage >= displayedPageNumbers + 1 &&
    lastPage - (displayedPageNumbers - 1) > activePage
  ) {
    startPagination = activePage - displayedPageNumbers
    finishPagination = activePage + displayedPageNumbers
  } else {
    startPagination = lastPage - displayedPageNumbers * 2 + 1
  }

  pagination.push(
    <PaginationButton
      key={startPagination - 1}
      data-page-value={activePage === 1 ? activePage : activePage - 1}
      className='fa fa-caret-left fa-3x'
      aria-hidden='true'
    />
  )

  for (let i = Math.abs(startPagination); i <= finishPagination; i++) {
    pagination.push(
      <PaginationLink isActive={activePage === i} key={i} href='#!' data-page-value={i}>
        {i}
      </PaginationLink>
    )
  }

  pagination.push(
    <PaginationButton
      key={finishPagination + 1}
      data-page-value={activePage === lastPage ? lastPage : activePage + 1}
      className='fa fa-caret-right fa-3x'
      aria-hidden='true'
    />
  )

  return pagination
}

const Pagination: React.FC<IPaginationProps> = ({ lastPage, activePage }) => (
  <>{renderPagination(lastPage, activePage)}</>
)

export default Pagination
