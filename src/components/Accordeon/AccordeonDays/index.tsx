import * as React from 'react'
import * as Redux from 'redux'
import styled, { css } from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { getActiveDay } from '../../../redux/selectors'
import { RootActionsType } from '../../../redux/redux-store'
import { menuActions } from '../../../redux/actions/menuAction'
import { MENU_ITEMS } from '../../../constants'
import colors from '../../../shared/styles/colors'

const Header = styled.h3`
  display: flex;
  justify-content: center;
  width: 100px;
  color: ${colors.white500};
  font-style: normal;
  font-weight: bold;
  font-size: 1.2rem;
`
const TitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0.6rem;
  margin-bottom: 0.2rem;
  align-items: center;
  height: 3rem;
  background-color: rgba(51, 51, 51, 0.9);
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
  border-radius: 6px;
  cursor: pointer;
`
interface IActive {
  isOpen?: boolean
}
const ArrowWrapper = styled.div`
  width: 1.4rem;
  height: 1.4rem;
  background-color: ${colors.white500};
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
  border-radius: 6px;
  font-size: 1.2rem;
  padding-top: 0.2rem;
  display: flex;
  align-items: center;
  justify-content: center;
`
const Arrow = styled.div<IActive>`
  ${({ isOpen }) => isOpen && ArrowActive};
`

const ArrowActive = css`
  padding: 0.3rem 0 0;
  transform: rotateX(180deg);
`

const Wrapper = styled.div`
  z-index: 1;
  height: 50%;
`
const DaysWrapper = styled.div`
  background-color: rgba(255, 255, 255, 0.95);
  color: rgb(51, 51, 51);
  border-radius: 6px;
`
const DayItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 3rem;
  font-style: normal;
  font-weight: bold;

  font-size: 1.2rem;
  text-align: center;
  cursor: pointer;
`

const AccordeonDays: React.FC = () => {
  const { changeActiveDay } = menuActions
  const dispatch = useDispatch<Redux.Dispatch<RootActionsType>>()
  const activeDay = useSelector(getActiveDay)

  const [isOpen, setOpen] = React.useState<boolean>(false)

  const [day] = MENU_ITEMS.filter((item) => item.id === activeDay)

  const onClickHandler = () => {
    dispatch(changeActiveDay(day.id))
    setOpen(!isOpen)
  }
  return (
    <Wrapper>
      <TitleWrapper onClick={() => setOpen(!isOpen)}>
        <Header>{day.label}</Header>
        <ArrowWrapper>
          <Arrow isOpen={isOpen}>
            <i className='fas fa-angle-down'></i>
          </Arrow>
        </ArrowWrapper>
      </TitleWrapper>
      <DaysWrapper>
        {isOpen
          ? MENU_ITEMS.map((day, key: number) => {
              return (
                <DayItem onClick={onClickHandler} key={key}>
                  {day.label}
                </DayItem>
              )
            })
          : null}
      </DaysWrapper>
    </Wrapper>
  )
}

export default AccordeonDays
