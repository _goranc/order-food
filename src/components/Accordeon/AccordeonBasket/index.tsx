import * as React from 'react'
import * as Redux from 'redux'
import { useDispatch } from 'react-redux'
import styled, { css } from 'styled-components'
import { actionsBasket } from '../../../redux/actions/basketAction'
import { RootActionsType } from '../../../redux/redux-store'
import { EDay } from '../../../mock-data/days'
import colors from '../../../shared/styles/colors'

const Header = styled.h3`
  color: ${colors.white500};
  font-style: normal;
  font-weight: bold;
  font-size: 1.2rem;
`
const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 8px 20px 12px;
  display: flex;
  align-items: center;
  height: 40px;
  background-color: rgba(51, 51, 51, 0.9);
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
  border-radius: 6px;
  margin-bottom: 10px;
  ::last-child {
    margin-bottom: 0;
  }
`
interface IActive {
  isOpen?: boolean
}
const ArrowWrapper = styled.div`
  width: 1.6rem;
  height: 1.6rem;
  background-color: ${colors.white500};
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
  border-radius: 6px;
  font-size: 1.4rem;
  display: flex;
  padding-top: 0.4rem;
  align-items: center;
  justify-content: center;
`
const Arrow = styled.div<IActive>`
  ${({ isOpen }) => isOpen && ArrowActive};
`

const ArrowActive = css`
  padding: 0.4rem 0 0;
  transform: rotateX(180deg);
`

type OwnPropsType = {
  day: { id: EDay; label: string }
  isOpen: boolean
  children: React.ReactNode
}

type TProps = OwnPropsType & IActive

const AccordeonBasket: React.FC<TProps> = ({ day, isOpen, children }) => {
  const { toogleAccordeon } = actionsBasket
  const dispatch = useDispatch<Redux.Dispatch<RootActionsType>>()

  return (
    <div>
      <HeaderWrapper onClick={() => dispatch(toogleAccordeon(day.id))}>
        <Header>{day.label}</Header>
        <ArrowWrapper>
          <Arrow isOpen={isOpen}>
            <i className='fas fa-angle-down'></i>
          </Arrow>
        </ArrowWrapper>
      </HeaderWrapper>
      {isOpen ? children : null}
    </div>
  )
}

export default AccordeonBasket
