import * as React from 'react'
import * as Redux from 'redux'
import styled from 'styled-components'
import { useDispatch } from 'react-redux'
import { RootActionsType } from '../../redux/redux-store'
import { menuActions } from '../../redux/actions/menuAction'
import { actionsBasket } from '../../redux/actions/basketAction'
import colors from '../../shared/styles/colors'

export const CounterWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-left: 1rem;
`

export const CounterButton = styled.button`
  width: 1.5rem;
  font-weight: 600;
  color: #3b3b3b;
  border: none;
  background-color: white;
  box-shadow: 0px 1px 4px ${colors.gray500};
  border-radius: 4px;
  height: 1.5rem;
`

export const CounterValue = styled.span`
  display: block;
  text-align: center;
  font-weight: bold;
  width: 1.4rem;
  height: 1.4rem;
`

type TProps = {
  name: string
  counter: number
  isBasket: boolean
}

export const Counter: React.FC<TProps> = ({ name, counter, isBasket }) => {
  const { changeMenuCounter } = menuActions
  const { changeBasketCounter } = actionsBasket
  const dispatch = useDispatch<Redux.Dispatch<RootActionsType>>()

  const onReduceHandler = () => {
    let value = counter > 1 ? counter - 1 : 1
    if (isBasket) {
      dispatch(changeBasketCounter(name, value))
      dispatch(changeMenuCounter(name, value))
      return
    }
    dispatch(changeMenuCounter(name, value))
  }

  const onAddHandler = () => {
    let value = counter < 9 ? counter + 1 : 9
    if (isBasket) {
      dispatch(changeBasketCounter(name, value))
      dispatch(changeMenuCounter(name, value))
      return
    }
    dispatch(changeMenuCounter(name, counter + 1))
  }

  return (
    <CounterWrapper>
      <CounterButton onClick={onReduceHandler}>-</CounterButton>
      <CounterValue style={{ padding: '0 3px' }}>{counter}</CounterValue>
      <CounterButton onClick={onAddHandler}>+</CounterButton>
    </CounterWrapper>
  )
}

export default Counter
