import * as React from 'react'
import * as Redux from 'redux'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { TBasketItem } from '../../types/basket-item'
import CopyToClipboard from 'react-copy-to-clipboard'
import { DAY_ABBREVIATION } from '../../mock-data/days'
import { RootActionsType } from '../../redux/redux-store'
import { getTotalItemsInBasket, getTotalCost, getBasketItems } from '../../redux/selectors'
import { actionsBasket } from '../../redux/actions/basketAction'
import { menuActions } from '../../redux/actions/menuAction'
import colors from '../../shared/styles/colors'

const Submit = styled.div`
  width: 8.4rem;
  box-shadow: 0px 1px 4px ${colors.gray500};
  background-color: ${colors.pink500};
  border-radius: 8px;
  font-weight: bold;
  text-align: center;
  line-height: 3.6rem;
  cursor: pointer;
`
const Wrapper = styled.div`
  width: 100%;
  height: 3.6rem;
  display: flex;
  margin: 1.4em auto;
  padding: 0 1.6rem;
  justify-content: space-between;
`

const WrapperTotalSection = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  max-width: 380px;
  box-shadow: 0px 1px 4px ${colors.gray500};
  border-radius: 8px;
  margin-right: 3rem;
  @media (max-width: 800px) {
    max-width: 280px;
    justify-content: space-between;
  }
`
const Total = styled.p`
  padding: 1.2rem 0.8rem;
  text-align: center;
`
type TCreateStringifyBasket = (basket: TBasketItem[]) => string

const createStringifyBasket: TCreateStringifyBasket = (basket) => {
  let day = ''
  return basket
    .sort((a, b) => (a.day > b.day ? 1 : -1))
    .map((item: TBasketItem) => {
      if (item.day === day) return item.menu_id + 'x' + item.counter
      day = item.day
      return '\n' + DAY_ABBREVIATION[item.day] + ': ' + item.menu_id + 'x' + item.counter
    })
    .join('/')
}

const Footer: React.FC = () => {
  const { submitBasket } = actionsBasket
  const { setMenuInitialState } = menuActions

  const dispatch = useDispatch<Redux.Dispatch<RootActionsType>>()
  const itemsInBasket = useSelector(getTotalItemsInBasket)
  const totalCost = useSelector(getTotalCost)
  const basket = useSelector(getBasketItems)

  const onCopyHandler = () => {
    dispatch(submitBasket())
    dispatch(setMenuInitialState())
  }

  return (
    <Wrapper>
      <WrapperTotalSection>
        <Total>
          Total price:&nbsp;&nbsp;<strong>{totalCost} uah</strong>
        </Total>
        <Total>
          Кол-во блюд:&nbsp;&nbsp;<strong>{itemsInBasket}</strong>
        </Total>
      </WrapperTotalSection>
      <CopyToClipboard text={createStringifyBasket(basket)} onCopy={onCopyHandler}>
        <Submit>Submit</Submit>
      </CopyToClipboard>
    </Wrapper>
  )
}

export default Footer
