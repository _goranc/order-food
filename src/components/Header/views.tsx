import styled from 'styled-components'
import colors from '../../shared/styles/colors'

export const FiltersWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

export const HeaderContent = styled.div`
  min-width: 320px;
  width: 60vw;
  padding: 1.6rem 0 3.4rem;
  display: flex;
  margin: 0 auto;
  justify-content: space-between;
  align-items: center;
  @media (max-width: 800px) {
    justify-content: center;
    width: 100vw;
  }
`

export const HeaderWrapper = styled.div`
  display: none;
  @media (max-width: 800px) {
    display: flex;
    height: 6rem;
    min-height: 6rem;
    background-color: ${colors.pink500};
    padding: 0 2rem;
    align-items: center;
    justify-content: space-between;
  }
`

export const LogoWrapper = styled.div`
  img {
    object-fit: cover;
    width: 120px;
  }
`
export const BasketWrapper = styled.div`
  @media (max-width: 800px) {
    display: none;
  }
`

export const ResetFilters = styled.div`
  width: 8rem;
  margin-right: 2rem;
  height: 2.8rem;
  box-shadow: 0px 1px 4px ${colors.gray500};
  background-color: ${colors.pink500};
  border-radius: 8px;
  font-weight: bold;
  text-align: center;
  line-height: 2.8rem;
  cursor: pointer;
`

export const DessertTab = styled.button`
  width: 8rem;
  height: 2.8rem;
  box-shadow: 0px 1px 4px ${colors.gray500};
  background-color: ${colors.white500};
  border-radius: 8px;
  border: none;
  text-align: center;
  line-height: 2.8rem;
  cursor: pointer;
`

export const customStyles = {
  option: (provided: any, state: any) => ({
    ...provided,
    color: `${colors.dark500}`,
    cursor: 'pointer',
    backgroundColor: state.isSelected ? `${colors.pink500}` : state.isFocused ? '#E5E5E5' : `${colors.white500}`,
  }),
  container: (provided: any) => ({
    ...provided,
    marginRight: '2rem',
  }),
  menuList: (provided: any) => ({
    ...provided,
    marginTop: 10,
  }),
  indicatorSeparator: () => ({}),
  menu: (provided: any) => ({
    ...provided,
    width: '9rem',
    borderRadius: 8,
    boxShadow: `0px 1px 4px ${colors.gray500}`,
  }),

  control: (provided: any) => ({
    ...provided,
    boxShadow: `0px 1px 4px ${colors.gray500}`,
    border: 'none',
    height: '2.8rem',
    minHeight: '2.8rem',
    borderRadius: 8,
    width: '9rem',

    cursor: 'pointer',
  }),
  singleValue: (provided: any, state: any) => {
    const opacity = state.isDisabled ? 0.5 : 1
    const transition = 'opacity 300ms'

    return { ...provided, opacity, transition }
  },
}
