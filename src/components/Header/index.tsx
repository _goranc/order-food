import * as React from 'react'
import { connect } from 'react-redux'
import * as Redux from 'redux'
import {
  HeaderWrapper,
  FiltersWrapper,
  HeaderContent,
  ResetFilters,
  customStyles,
  DessertTab,
  LogoWrapper,
  BasketWrapper,
} from './views'
import AccordeonDays from '../Accordeon/AccordeonDays'
import BasketModal from '../BasketModal'
import Select from 'react-select'
import { menuActions } from '../../redux/actions/menuAction'
import { StateType, RootActionsType } from '../../redux/redux-store'
import { getFilter } from '../../redux/selectors'
import { MAIN_DISHES_OPTION } from '../../mock-data/food/select-options'

type TFilterItems = (event?: any) => void

type MapStatePropsType = {
  filter: string
}
type MapDispatchPropsType = {
  addFilter: TFilterItems
}
type TProps = MapStatePropsType & MapDispatchPropsType

const Header: React.FC<TProps> = ({ addFilter, filter }) => {
  return (
    <>
      <HeaderWrapper>
        <LogoWrapper>
          <img src='/logo.png' alt='logo' />
        </LogoWrapper>
        <AccordeonDays></AccordeonDays>
        <BasketModal></BasketModal>
      </HeaderWrapper>
      <HeaderContent>
        <FiltersWrapper>
          <ResetFilters onClick={() => addFilter()}>Сбросить</ResetFilters>
          <Select
            value={{ value: 'Основные', label: filter || 'Основные' }}
            onChange={(event) => addFilter(event)}
            options={MAIN_DISHES_OPTION}
            styles={customStyles}
          />
          <DessertTab onClick={(event: React.MouseEvent<HTMLElement>) => addFilter(event)}>
            Десерты
          </DessertTab>
        </FiltersWrapper>
        <BasketWrapper>
          <BasketModal></BasketModal>
        </BasketWrapper>
      </HeaderContent>
    </>
  )
}

const mapStateToProps = (state: StateType) => ({ filter: getFilter(state) })

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootActionsType>): MapDispatchPropsType => ({
  addFilter: (event: any) => dispatch(menuActions.addFilter(event)),
})

export default connect<MapStatePropsType, MapDispatchPropsType, {}, StateType>(
  mapStateToProps,
  mapDispatchToProps
)(Header)
