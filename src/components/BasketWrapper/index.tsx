import * as React from 'react'
import styled from 'styled-components'
import { useSelector } from 'react-redux'
import { getTotalItemsInBasket } from '../../redux/selectors'
import colors from '../../shared/styles/colors'

const Wrapper = styled.div`
  width: 3rem;
  height: 3rem;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  box-shadow: 0px 1px 4px ${colors.gray500};
  border-radius: 8px;
  cursor: pointer;
`
const Count = styled.span`
  color: ${colors.white500};
  font-size: 0.8rem;
  font-weight: 800;
  line-height: 1rem;
  text-align: center;
  width: 1.5rem;
  height: 1rem;
  border-radius: 40%;
  background-color: #ff6060;
  position: absolute;
  top: 10%;
  right: 8%;
`
const Icon = styled.i`
  color: #2d2e30;
`

type TProps = {
  onClick: () => void
}

const BasketWrapper: React.FC<TProps> = ({ onClick }) => {
  const itemsInBasket = useSelector(getTotalItemsInBasket)

  return (
    <Wrapper onClick={onClick}>
      <Count>{itemsInBasket}</Count>
      <Icon className='fa fa-shopping-basket fa-lg' aria-hidden='true'></Icon>
    </Wrapper>
  )
}

export default BasketWrapper
