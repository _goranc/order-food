import React from 'react'
import * as Redux from 'redux'
import { connect } from 'react-redux'
import { TBasketItem } from '../../types/basket-item'
import {
  customStyles,
  ModalBasket,
  WrapperModal,
  WrapperContent,
  Label,
  FieldName,
  WrapperValue,
  WrapperItemContent,
  HeaderWrapper,
  Button,
  RemoveButton,
  TotalPrice,
  TotalPriceHeader,
  TotalPriceValue,
} from './views'
import Basket from '../BasketWrapper'
import colors from '../../shared/styles/colors'
import AccordeonBasket from '../Accordeon/AccordeonBasket'
import { EDay } from '../../mock-data/days'
import { AccordeonItem } from '../../redux/reducers/menuReducer'
import { actionsBasket } from '../../redux/actions/basketAction'
import { StateType, RootActionsType } from '../../redux/redux-store'
import { getBasketItems, getAccordeon, getTotalCost, getActiveDay } from '../../redux/selectors'
import { Counter } from '../Counter'
import { MENU_ITEMS } from '../../constants'

type MapStatePropsType = {
  accordeon: AccordeonItem[]
  basket: TBasketItem[]
  totalCost: number
  activeDay: EDay
}

type MapDispatchPropsType = {
  removeFromBasket: (name: string) => void
  toogleAccordeon: (dayName: string) => void
}

type TProps = MapStatePropsType & MapDispatchPropsType

const BasketModal: React.FC<TProps> = (props) => {
  const { basket, accordeon, totalCost, activeDay, removeFromBasket, toogleAccordeon } = props

  let subtitle: any
  const [modalIsOpen, setIsOpen] = React.useState(false)
  const openModal = () => {
    toogleAccordeon(activeDay)
    setIsOpen(true)
  }
  const afterOpenModal = () => (subtitle.style.color = colors.dark500)
  const closeModal = () => setIsOpen(false)

  return (
    <div>
      <Basket onClick={openModal}>Open Modal</Basket>
      <ModalBasket
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel='Example Modal'
      >
        <WrapperModal>
          <Button onClick={closeModal}> &times;</Button>
          <HeaderWrapper>
            <h1 ref={(_subtitle) => (subtitle = _subtitle)}>Your order</h1>
          </HeaderWrapper>

          <WrapperContent>
            {MENU_ITEMS.map((day, id: number) => {
              const [accordeonItem] = accordeon.filter(
                (item: AccordeonItem) => day.id === item.dayName
              )
              const itemsOfDay = basket
              .sort((a, b) => (a.menu_id > b.menu_id ? 1 : -1))
              .map((item: TBasketItem, id: any) => {
                if (day.id === item.day) {
                  return (
                    <div key={id}>
                      <WrapperItemContent>
                        <Label>{item.name}</Label>
                        <WrapperValue>
                          <p>
                            <FieldName>id: </FieldName>
                            <strong>{item.menu_id}</strong>
                          </p>
                          <p>
                            <FieldName>price: </FieldName>
                            <strong>{item.price} uah</strong>
                          </p>
                        </WrapperValue>
                        <Counter name={item.name} counter={item.counter} isBasket={true} />
                        <div>
                          <RemoveButton onClick={() => removeFromBasket(item.name)}>
                            &times;
                          </RemoveButton>
                        </div>
                      </WrapperItemContent>
                    </div>
                  )
                } else {
                  return null
                }
              })

              return (
                <AccordeonBasket day={day} isOpen={accordeonItem.isOpen} key={id}>
                  {itemsOfDay}
                </AccordeonBasket>
              )
            })}
          </WrapperContent>
          <TotalPrice>
            <TotalPriceHeader>Общая стоимость:&nbsp;</TotalPriceHeader>
            <TotalPriceValue>{totalCost} uah</TotalPriceValue>
          </TotalPrice>
        </WrapperModal>
      </ModalBasket>
    </div>
  )
}

const mapStateToProps = (state: StateType): MapStatePropsType => {
  return {
    basket: getBasketItems(state),
    accordeon: getAccordeon(state),
    totalCost: getTotalCost(state),
    activeDay: getActiveDay(state),
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootActionsType>): MapDispatchPropsType => {
  return {
    removeFromBasket: (name: string) => dispatch(actionsBasket.removeFromBasket(name)),
    toogleAccordeon: (dayName: string) => dispatch(actionsBasket.toogleAccordeon(dayName)),
  }
}

export default connect<MapStatePropsType, MapDispatchPropsType, {}, StateType>(
  mapStateToProps,
  mapDispatchToProps
)(BasketModal)
