import styled from 'styled-components'
import Modal from 'react-modal'
import colors from '../../shared/styles/colors'

Modal.setAppElement('#root')

export const customStyles = {
  overlay: { zIndex: 3 },
}

export const ModalBasket = styled(Modal)`
  position: absolute;
  top: 50%;
  left: 50%;
  right: auto;
  bottom: auto;
  margin-right: -50%;
  width: 650px;
  background: ${colors.pink500};
  border-radius: 8px;
  transform: translate(-50%, -50%);
  @media (max-width: 800px) {
    min-height: 300px;
    max-width: 100%;
    min-width: 300px;
  }
`

export const WrapperModal = styled.div`
  font-family: 'Roboto', sans-serif;
  background: ${colors.pink500};
  width: 100%;
  overflow: hidden;
  padding: 1rem;
  border-radius: 8px;
`
export const WrapperContent = styled.div`
  overflow: auto;
  padding-right: 0.4em;
  min-width: 330px;
`

export const Label = styled.label`
  width: 100%;
  padding-left: 0.8em;
`

export const FieldName = styled.span`
  padding-left: 1rem;
  color: rgba(51, 51, 51, 0.5);
`

export const WrapperValue = styled.div`
  width: 260px;
  min-width: 180px;
  display: flex;

  @media (max-width: 800px) {
    flex-direction: column;
    width: 160px;
    min-width: 100px;
  }
`
export const WrapperItemContent = styled.li`
  background: rgba(255, 255, 255, 0.9);
  display: flex;
  align-items: center;
  margin-bottom: 0.8em;
  padding: 0.8em 1em;
  color: ${colors.dark500};
  border-radius: 6px;
  font-size: 1.2em;
  line-height: 18px;
`

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: center;
`

export const Button = styled.button`
  position: relative;
  left: calc(100% - 2rem);
  top: 0;
  width: 1.6rem;
  height: 1.6rem;
  line-height: 0.6rem;
  font-size: 1.6rem;
  color: ${colors.white500};
  box-shadow: 0px 1px 4px ${colors.gray500};
  border-radius: 5px;
  cursor: pointer;
  background-color: rgba(51, 51, 51, 0.9);
  border: none;
`
export const RemoveButton = styled.button`
  width: 20px;
  height: 20px;
  margin-left: 1.6em;
  line-height: 1em;
  color: black;
  font-weight: bold;
  box-shadow: 0px 1px 4px ${colors.gray500};
  border-radius: 5px;
  background-color: ${colors.white500};
  border: none;
  cursor: pointer;
`

export const TotalPrice = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 2rem;
  align-items: center;
`
export const TotalPriceHeader = styled.h1`
  font-size: 1.2rem;
  color: rgba(51, 51, 51, 0.7);
`
export const TotalPriceValue = styled.strong`
  font-family: Merriweather Sans;
  font-size: 1rem;
`
