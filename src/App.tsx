import * as React from 'react'
// styles
import GlobalStyles from './shared/styles/global'
// components
import Sidebar from './components/sidebar'
// context
import { ActiveDayProvider } from './shared/context/active-day'
import Footer from './components/Footer'
import MenuList from './components/MenuList'
import styled from 'styled-components'
import Header from './components/Header'

const Wrapper = styled.div`
  display: flex;
`

export const WrapperPage = styled.div`
  display: flex;
  height: 100vh;
  margin: 0 auto;
  flex-direction: column;
  justify-content: center;
`

const App: React.FC = () => (
  <ActiveDayProvider>
    <Wrapper>
      <GlobalStyles />
      <Sidebar></Sidebar>
      <WrapperPage>
        <Header></Header>
        <MenuList />
        <Footer></Footer>
      </WrapperPage>
    </Wrapper>
  </ActiveDayProvider>
)

export default App
