import { EDay } from '../mock-data/days'

export interface TBasketItem {
  name: string
  menu_id: number
  price: number
  second_id?: number
  counter: number
  day: EDay
}
