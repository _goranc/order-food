import { EDay } from '../mock-data/days'

export interface TMenuItem {
  days_week: Array<EDay>
  menu_id: number
  name: string
  price: number
  menu_type?: string
  second_id?: number
  counter: number
  isChecked: boolean
}
