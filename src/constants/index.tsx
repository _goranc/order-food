import { EDay } from '../mock-data/days'

export const MENU_ITEMS = [
  { id: EDay.MONDAY, label: 'Monday' },
  { id: EDay.TUESDAY, label: 'Tuesday' },
  { id: EDay.WEDNESDAY, label: 'Wednesday' },
  { id: EDay.THURSDAY, label: 'Thursday' },
  { id: EDay.FRIDAY, label: 'Friday' },
]
