import * as React from 'react'
import { EDay } from '../../../mock-data/days'
import { useDispatch } from 'react-redux'

interface IContext {
  activeDay: EDay
  setActiveDay: (day: EDay) => void
}

export const ActiveDayContext = React.createContext<IContext>({} as IContext)

export const ActiveDayProvider: React.FC = ({ children }) => {
  const [day, setDay] = React.useState<EDay>(EDay.MONDAY)
  const dispatch = useDispatch()

  const setDayInStore = (day: EDay) => {
    setDay(day)
    dispatch({ type: 'CHANGE_ACTIVE_DAY', day })
  }

  const ctxValue = React.useMemo(
    () => ({
      activeDay: day,
      setActiveDay: (day: EDay) => setDayInStore(day),
    }),
    // eslint-disable-next-line
    [day]
  )

  return <ActiveDayContext.Provider value={ctxValue}>{children}</ActiveDayContext.Provider>
}
