import * as React from 'react'
import styled, { css } from 'styled-components'
import colors from '../../styles/colors'

interface IActive {
  checked?: boolean
}

const Option = styled.label`
  display: block;
`
const CHECK_DEFAULT = css`
  position: absolute;
  left: -16px;
  width: 16px;
  height: 16px;
  background: ${colors.white500};
  border: 1px solid rgba(51, 51, 51, 0.25);
  border-radius: 3px;
  background-repeat: no-repeat;
`
const ARROW_CHECKED = css`
  background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='13' height='15' viewBox='0 0 24 24'%3E%3Cpath d='M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z'/%3E%3C/svg%3E");
`
const CheckInput = styled.input<IActive>`
  position: absolute;
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
  border: none;
  ::before {
    content: '';
    ${CHECK_DEFAULT}
  }

  :checked {
    ::before {
      content: '';
      ${CHECK_DEFAULT}
      ${({ checked }) => checked && ARROW_CHECKED};
    }
  }
`
interface IProps {
  onChange: () => void
  checked: boolean
  name: string
}
const CheckBox = (props: IProps) => {
  const { onChange, checked, name } = props
  return (
    <Option>
      <CheckInput onChange={onChange} type='checkbox' checked={checked} />
      &nbsp;&nbsp;{name}
    </Option>
  )
}

export default CheckBox
