import * as React from 'react'
import styled, { css } from 'styled-components'

interface IActive {
  isActive?: boolean
}

const ACTIVE_STYLES = css`
  background: #ffffff;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);

  :hover {
    opacity: 1;
    box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.25);
  }
`

// views
export const MenuItemWrapper = styled.div<IActive>`
  font-size: 1.2rem;
  font-weight: bold;
  border-radius: 6px;
  width: 136px;
  padding: 8px 16px;
  text-align: center;
  transition: all 0.3s ease-in-out;
  cursor: pointer;

  :hover {
    background: #ffffff;
    box-shadow: inset 0px 1px 4px rgba(0, 0, 0, 0.25);
  }

  ${({ isActive }) => isActive && ACTIVE_STYLES};
`

// component
interface IProps {
  label: string
  onClick?: () => void
}

type TProps = IProps & IActive

const MenuItem: React.FC<TProps> = ({ label, onClick, isActive }) => (
  <MenuItemWrapper onClick={onClick} isActive={isActive}>
    {label}
  </MenuItemWrapper>
)

MenuItem.defaultProps = {
  label: 'Default',
}

export default MenuItem
