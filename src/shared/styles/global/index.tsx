import { createGlobalStyle } from 'styled-components'
// css
// @ts-ignore
import normalize from 'normalize.css'

const GlobalStyles = createGlobalStyle`
  ${normalize};

  @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap');

  html, body, #root {
    min-height: 100vh;
    font-family: 'Roboto', sans-serif;
    -webkit-tap-highlight-color: transparent;
    font-size: 14px;
    @media (max-width: 800px) {
    font-size: 12px;
  }
  }

  * { 
    box-sizing: border-box;
    outline: none;
  }
  
  *, ::before, ::after {
    box-sizing: border-box;
    outline: none;
  }
`

export default GlobalStyles
