export default {
  pink500: '#FDE5E5',
  white500: '#fff',
  gray500: '#BABABA',
  dark500: '#333'
}