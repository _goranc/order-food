import { TBasketItem } from '../../types/basket-item'
import { ActionTypesBasket } from '../actions/basketAction'
import { EDay } from '../../mock-data/days'
import { AccordeonItem } from './menuReducer'

const initialState = {
  basket: [] as Array<TBasketItem>,
  accordeon: Object.keys(EDay).map((el) => ({ dayName: el, isOpen: false })) as AccordeonItem[],
}
export type InitialStateType = typeof initialState

export const basketReducer = (
  state = initialState,
  action: ActionTypesBasket
): InitialStateType => {
  switch (action.type) {
    case 'ADD_TO_BASKET':
      return {
        ...state,
        basket: [...state.basket, action.basketItem],
      }
    case 'REMOVE_FROM_BASKET':
      return {
        ...state,
        basket: state.basket.filter((elem: TBasketItem) => elem.name !== action.name),
      }
    case 'CHANGE_COUNTER_BASKET':
      return {
        ...state,
        basket: state.basket.map((item) =>
          item.name === action.name
            ? {
                ...item,
                counter: action.newCounter,
                price: (item.price / item.counter) * action.newCounter,
              }
            : item
        ),
      }
    case 'SUBMIT_BASKET':
      alert('Корзина была успешно скопирована.')
      return {
        ...state,
        basket: [],
        accordeon: Object.keys(EDay).map((el) => ({
          dayName: el,
          isOpen: false,
        })) as AccordeonItem[],
      }
    case 'CHANGE_ACCORDEON':
      return {
        ...state,
        accordeon: state.accordeon.map((item: AccordeonItem) =>
          item.dayName === action.dayName ? { ...item, isOpen: !item.isOpen } : item
        ),
      }
    default:
      return state
  }
}
