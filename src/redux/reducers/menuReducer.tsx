import { MENU_SEEDS } from '../../mock-data/food/menu'
import { EDay } from '../../mock-data/days'
import { ActionTypesMenu } from '../actions/menuAction'

export type AccordeonItem = { dayName: string; isOpen: boolean }

const initialState = {
  foodMenu: MENU_SEEDS.map((item) =>  ({ ...item, isChecked: false, counter: 1 })),
  page: 1,
  filter: '',
  activeDay: EDay.MONDAY,
}
export type InitialStateType = typeof initialState

export const menuReducer = (state = initialState, action: ActionTypesMenu): InitialStateType => {
  switch (action.type) {
    case 'CHANGE_PAGE':
      return {
        ...state,
        page: action.page,
      }
    case 'FILTER_ADD':
      return {
        ...state,
        filter: action.filter,
      }
    case 'CHANGE_COUNTER_MENU':
      return {
        ...state,
        foodMenu: state.foodMenu.map((item) =>
          item.name === action.name ? { ...item, counter: action.newCounter } : item
        ),
      }
    case 'TOGGLE_CHECKED':
      return {
        ...state,
        foodMenu: state.foodMenu.map((item) =>
          item.name === action.menuItem.name ? { ...item, isChecked: !item.isChecked } : item
        ),
      }
    case 'CHANGE_ACTIVE_DAY':
      return {
        ...state,
        activeDay: action.day,
      }
      case 'SET_INITIAL_STATE':
        return {
          ...state,
          foodMenu: MENU_SEEDS.map((item) =>  ({ ...item, isChecked: false, counter: 1 })),
          activeDay: EDay.MONDAY,
        }
    default:
      return state
  }
}
