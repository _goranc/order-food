import { StateType } from './redux-store'
import { createSelector } from 'reselect'

export const getMenuItems = (state: StateType) => state.menuReducer.foodMenu

export const getBasketItems = (state: StateType) => state.basketReducer.basket

export const getTotalItemsInBasket = createSelector(getBasketItems, (basket) => basket.length)

export const getTotalCost = createSelector(getBasketItems, (basket) =>
  basket.reduce((sum, curr) => sum + curr.price, 0)
)

export const getPage = (state: StateType) => state.menuReducer.page

export const getFilter = (state: StateType) => state.menuReducer.filter

export const getActiveDay = (state: StateType) => state.menuReducer.activeDay

export const getAccordeon = (state: StateType) => state.basketReducer.accordeon

const getMenuItemsFiltredDay = createSelector([getActiveDay, getMenuItems], (activeDay, foodMenu) =>
  foodMenu.filter((item) => item.days_week.includes(activeDay))
)

export const getMenuItemsFiltred = createSelector(
  [getFilter, getMenuItemsFiltredDay],
  (filter, foodMenu) => (filter ? foodMenu.filter((item) => item.menu_type === filter) : foodMenu)
)
