import { combineReducers } from 'redux'
import { menuReducer } from './reducers/menuReducer'
import { basketReducer } from './reducers/basketReducer'
import { createStore as createReduxStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { ActionTypesMenu } from './actions/menuAction'
import { ActionTypesBasket } from './actions/basketAction'
import Window from '../../declarations'

export const rootReducer = combineReducers({
  basketReducer,
  menuReducer,
})

type RootReducerType = typeof rootReducer // (globalstate: StateType) => StateType
export type StateType = ReturnType<RootReducerType>

export type InferActionsTypes<T> = T extends { [key: string]: infer U } ? U : never
export type RootActionsType = ActionTypesMenu | ActionTypesBasket

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose

export const store = createReduxStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))
