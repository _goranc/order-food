import { InferActionsTypes } from '../redux-store'
import { TMenuItem } from '../../types/menu-item'
import { TBasketItem } from '../../types/basket-item'
import { EDay } from '../../mock-data/days'

export type ActionTypesBasket = ReturnType<InferActionsTypes<typeof actionsBasket>>

export const actionsBasket = {
  addToBasket: (menuItem: TMenuItem, day: EDay) => {
    const { name, menu_id, price, counter } = menuItem
    let basketItem = {
      name,
      menu_id,
      price: price * counter,
      second_id: !!menuItem.second_id ? menuItem.second_id : undefined,
      counter,
      day,
    } as TBasketItem
    return {
      type: 'ADD_TO_BASKET',
      basketItem,
      menuItem,
    } as const
  },
  removeFromBasket: (name: string) =>
    ({
      type: 'REMOVE_FROM_BASKET',
      name,
    } as const),
  submitBasket: () => ({ type: 'SUBMIT_BASKET' } as const),
  changeBasketCounter: (name: string, newCounter: number) => {
    return {
      type: 'CHANGE_COUNTER_BASKET',
      name,
      newCounter,
    } as const
  },
  toogleAccordeon: (dayName: string) =>
    ({
      type: 'CHANGE_ACCORDEON',
      dayName,
    } as const),
}
