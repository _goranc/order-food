import { InferActionsTypes } from '../redux-store'
import { TMenuItem } from '../../types/menu-item'
import { EDay } from '../../mock-data/days'

export type ActionTypesMenu = ReturnType<InferActionsTypes<typeof menuActions>>

export const menuActions = {
  changePage: (event: React.MouseEvent<HTMLElement>, currentPage: number) => {
    const dataAttr = (event.target as HTMLLinkElement).getAttribute('data-page-value')
    const targetPage = dataAttr ? Number(dataAttr) : currentPage

    return {
      type: 'CHANGE_PAGE',
      page: targetPage,
    } as const
  },
  addFilter: (event: any) => {
    let filter = ''
    try {
      filter = event.value || event.target.textContent
    } catch (e) {}
    return {
      type: 'FILTER_ADD',
      filter,
    } as const
  },
  changeMenuCounter: (name: string, newCounter: number) => {
    return {
      type: 'CHANGE_COUNTER_MENU',
      name,
      newCounter,
    } as const
  },
  toogleChecked: (menuItem: TMenuItem) => {
    return {
      type: 'TOGGLE_CHECKED',
      menuItem,
    } as const
  },
  changeActiveDay: (day: EDay) => {
    return {
      type: 'CHANGE_ACTIVE_DAY',
      day,
    } as const
  },
  setMenuInitialState: () => {
    return {
      type: 'SET_INITIAL_STATE',
    } as const
  },
}
